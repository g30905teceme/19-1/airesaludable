#include <ESP8266WiFi.h>
#include <WiFiClient.h>

int contconexion = 0;

const char *ssid = "ssid";
const char *password = "password";

unsigned long previousMillis = 0;

char host[48];
String strhost = "host";
String strurl = "urlservice";
String chipid = "";

String enviardatos(String datos) {
  String linea = "error";
  WiFiClient client;
  strhost.toCharArray(host, 49);
  if (!client.connect(host, 80)) {
    Serial.println("Fallo de conexion");
    return linea;
  }

  client.print(String("POST ") + strurl + " HTTP/1.1" + "\r\n" +
               "Host: " + strhost + "\r\n" +
               "Accept: */*" + "*\r\n" +
               "Content-Length: " + datos.length() + "\r\n" +
               "Content-Type: application/x-www-form-urlencoded" + "\r\n" +
               "\r\n" + datos);
  delay(10);

  Serial.print("Enviando datos...");
  while (client.available()) {
    linea = client.readStringUntil('\r');
  }
  Serial.println(linea);
  return linea;
}

//-------------------------------------------------------------------------

void setup() {

  Serial.begin(115200);
  Serial.print("Chip ID: ");
  chipid = String(ESP.getChipId());
  
  // Conexión WIFI
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED and contconexion < 50) {
    ++contconexion;
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi conectado");
  Serial.println(WiFi.localIP());
}

void loop() {

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= 10000) {
    previousMillis = currentMillis;
    int analog = analogRead(A0);
    Serial.println(analog);
    enviardatos("chipid=" + chipid + "&ppm=" + analog);
  }
}
